# NO 當用漢字 (NO 『当用漢字表』『同音の漢字による書きかえ』)

當用漢字、常用漢字表記をかえたいよ mozc 辭書。

- `mozc_kakikae.tsv`
  - mozc用の辭書data (tab separate)
  - 當用漢字が行った同音の漢字による書きかえを、本來の熟語にする。
  - tab separateにつき、自分のIME用に變更していただければ使えるでしょう。
    - windows用mac用のファイルを作る氣は無い。

- `mozc_traditional.tsv`
  - mozc用の辭書data (tab separate)
  - 當用漢字已前の表記

----
## mozc辭書data file

### `mozc_kakikae.tsv` フォーマット
- mozcに從う。tab separate。
  ```
  よみ	變換	mozc品詞	コメント
  ```

- コメント欄のフォーマット
  ```
  當用：入れかえになった字｜讀みガナ｜當用漢字、常用漢字での表記｜メモ
  ```
  - 「：」「｜」は全角文字。
  - 複數の字、表記がある場合は「，」全角カンマでつなげてある。


### `mozc_traditional.tsv` フォーマット
- mozcに從う。同上
- コメント欄のフォーマット
  ```
  trad：讀みガナ｜文字
  ```

----
## ライセンス
MIT
